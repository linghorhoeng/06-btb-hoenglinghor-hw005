// Class for send a message using Threads
class ThreadedSend extends Thread
{
    private String msg;
    private int second;

    ThreadedSend(String m,  int sc)
    {
        this.second=sc;
        this.msg=m;
    }

    public void run()
    {
        print(msg,second);

    }
    synchronized void print(String msg, int sec)
    {
        for (int i=0;i<msg.length();i++){
            try {
                sleep(sec);
                System.out.print(msg.charAt(i));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class Main {
    public static void main (String[] args)
    {
//        Sender snd = new Sender();
        ThreadedSend S1 = new ThreadedSend( " Hello KSHRD!\n" , 50 );
        ThreadedSend S2 = new ThreadedSend( "******************************************\n" +
                                                " I will try my best to be here at HRD. \n" +
                                                "------------------------------------------\n" , 200 );
        ThreadedSend S3 = new ThreadedSend( "Downloading" , 50 );
        ThreadedSend S4 = new ThreadedSend( "..........", 200 );
        ThreadedSend S5 = new ThreadedSend( "Completed 100%" , 50 );

        // wait for threads to end
        try
        {
            S1.start();
            S1.join();
            S2.start();
            S2.join();
            S3.start();
            S3.join();
            S4.start();
            S4.join();
            S5.start();
            S5.join();
        }
        catch(Exception e)
        {
            System.out.println("Interrupted");
        }
    }
}
